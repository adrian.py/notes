const express = require('express');
const path = require('path');
const { engine } = require('express-handlebars');
const methodOverride = require('method-override');
const session = require('express-session');
const flash = require('connect-flash');

//Inicializaciones
const app = express();
require('./database');

//Configuraciones
app.set('puerto', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.engine('.hbs', engine(
    {
        defaultLayout: 'main',
        defaultDir: path.join('views', 'layouts'),
        //partialsDir: path.join('views', 'partials'),
        partialsDir: __dirname + '/views/partials/',
        extname: 'hbs',
        runtimeOptions:{
            allowProtoPropertiesByDefault: true,
            //allowProtoMethodsByDefault: true
        },
    },
));
app.set('view engine', 'hbs');

//Middleware
app.use(express.urlencoded({extended:false}));
app.use(methodOverride('_method'));
app.use(session({
    secret: 'mclaren',
    resave: true,
    saveUninitialized: true
}))
app.use(flash());

//Variables Globales
app.use(function(request, response, next){
    response.locals.success_msg = request.flash('success_msg');
    response.locals.error_msg = request.flash('error_msg');
    next();
})

//Rutas
app.use(require('./routes/index'));
app.use(require('./routes/notes'));
app.use(require('./routes/users'));

//Archivos estaticos
app.use(express.static(path.join(__dirname, 'public')));

//Servidor
app.listen(app.get('puerto'), function () {
    console.log(`Servidor corriendo en el puerto ${app.get('puerto')}`);
});