const express = require('express');
const router = express.Router();

const Nota = require('../models/Notes');

router.get('/notes', async function(request, response){
    await Nota.find({}).lean().sort({fecha: 'desc'})
        .then((notas) => {
            /* console.log(notas); */
            response.render('notes/listado', {notas});
        })
        .catch((error) => {
            console.log(error);
        });
});

router.get('/notes/new', function(request, response){
    response.render('notes/nueva-nota');
});

router.get('/notes/edit/:id', async function(request, response){
    try{
        var _id = request.params.id;
        _id = _id.substring(1, _id.length);
        const nota = await Nota.findById(_id);
        _id = nota._id;
        titulo = nota.titulo;
        descripcion = nota.descripcion;
        response.render('notes/editar-nota', {nota, _id});
    }catch(error){
        response.sendStatus(404);
        response.redirect('/error');
    };
});

router.post('/notes/update/:id', async function(request, response){
    const {titulo, descripcion} = request.body;
    /* console.log(request.params.id)
    console.log(request.body); */
    await Nota.findByIdAndUpdate(request.params.id, {titulo, descripcion});
    response.redirect('/notes');
});


router.post('/notes/store', async function(request, response){
    /* console.log(request.body); */
    //response.send("Ok");
    const {titulo, descripcion} = request.body;
    const errors = [];

    if(!titulo){
        errors.push({text: "Por favor inserta el nombre"});
    }
    if(!descripcion){
        errors.push({text: "Por favor inserta la descripción"})
    }
    if(errors.length > 0){
        response.render('notes/nueva-nota', {
            errors,
            titulo,
            descripcion
        });
    }else{
        const newNota = new Nota({titulo, descripcion});
        await newNota.save()
            .then(() => {
                request.flash('success_msg', 'Nota agregada correctamente');
                response.redirect('/notes');
            })
            .catch((error) => {
                console.log(error);
                response.redirect('errors');
            })
    }
});

router.get('/notes/delete/:id', async function(request, response){
    try{
        /* console.log(request.params); */
        var _id = request.params.id;
        _id = _id.substring(1, _id.length);
        const nota = await Nota.findByIdAndDelete(_id);
        response.redirect('/notes');
    }catch(error){
        response.sendStatus(404);
        response.redirect('/error');
    }
});

module.exports = router;